variable "project" {
  description = "the name of the gcp project"
  type        = string
  default     = null

}

variable "name" {
  description = "Name of the cloud run service"
  type        = string
}

variable "location" {
  description = "location of the cloud run resource"
  type        = string
}

variable "ingress" {
  description = "controls the type of inbound traffic cloud run service can allow to access container"
  type        = string
  default     = "INGRESS_TRAFFIC_INTERNAL_ONLY"
}

variable "use_default" {
  description = "disable/enable binary authorization"
  type        = bool
}

variable "min_instance_count" {
  description = "minimum number of container instances"
  type        = number

}

variable "max_instance_count" {
  description = "maximum number of container instances"
  type        = number

}

variable "service_account" {
  description = "service account used to create this cloud run service"
  type        = string

}

variable "image" {
  description = "container image used in cloud run service"
  type        = string

}

variable "cpu" {
  description = "the cpu limit per container"
  type        = string
}

variable "memory" {
  description = "the memory limit per container"
  type        = string

}

variable "container_port" {
  description = "the exposed port of the container"
  type        = number

}