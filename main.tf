resource "google_cloud_run_v2_service" "default" {
  project  = var.project  #"mlops-dev-406321"
  name     = var.name     #"test-service"
  location = var.location #"us-central1"
  ingress  = var.ingress  #"INGRESS_TRAFFIC_ALL" # INGRESS_TRAFFIC_INTERNAL_ONLY, 
  # INGRESS_TRAFFIC_INTERNAL_LOAD_BALANCER 
  binary_authorization {
    use_default = var.use_default #false

  }
  template {
    scaling {
      min_instance_count = var.min_instance_count #1
      max_instance_count = var.max_instance_count #3

    }

    service_account = var.service_account #"gitlab-wif-demo@mlops-dev-406321.iam.gserviceaccount.com"

    containers {
      image = var.image #"gcr.io/mlops-dev-406321/test_api_image@sha256:17ccbadb00cd1d75c3a0a04c937dacd0b607add0ec95c63b7e392c27101b2254" #make sure you specify the image tag as well 
      resources {
        limits = {
          cpu    = var.cpu    #"2"
          memory = var.memory #"1024Mi"
        }
      }
      ports {
        container_port = var.container_port #8000
      }
    }

  }
}